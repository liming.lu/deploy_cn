#!/bin/bash
#. /home/ec2-user/deploycode/deploy_extra/conf/global.conf 
#remotedir='/home/ec2-user/'
#. /home/ec2-user/deploycode/deploy_extra/lib/*
#cd /home/ec2-user/deploycode/deploy_extra
work_dir="/home/ec2-user/deploycode/k9deployshell"
. $work_dir/conf/global.conf
. $work_dir/lib/*

cd $work_dir

#####################################################
##########     选择部署的主机	#####################
#####################################################
	echo "1) kick9_sbpre"
	echo "2) kick9_sandbox"
	echo "3) kick9_online"
	echo "4) kick9_ope"
	echo "5) kick9_dev"
read -p "choice deploy host 主机:" host
	[ "x$host" == "x1" ] && host='kick9_sbpre'
	[ "x$host" == "x2" ] && host='kick9_sandbox'
	[ "x$host" == "x3" ] && host='kick9_online'
	[ "x$host" == "x4" ] && host='kick9_ope'
	[ "x$host" == "x5" ] && host='kick9_dev'

#####################################################
##########     选择部署的区域	#####################
#####################################################
	echo "1) cn"
	echo "2) tc"
	echo "3) us"
read -p "choice deploy region 区域:" region
	[ "x$region" == "x1" ] && region='cn'
	[ "x$region" == "x2" ] && region='tc'
	[ "x$region" == "x3" ] && region='us'

#####################################################
##########     选择部署的应用	#####################
#####################################################
        echo "2) k9game(ope)"
        echo "3) k9forum_us(ope)"
        echo "5) k9wiki"
        echo "6) monster(ope)"
        echo "7) k9forum_cn(ope)"
read -p "choice deploy application 应用:" app
        [ "x$app" == "x2" ] && app='k9game'
        [ "x$app" == "x3" ] && app='k9forum_us'
        [ "x$app" == "x5" ] && app='k9wiki'
        [ "x$app" == "x6" ] && app='monster'
        [ "x$app" == "x7" ] && app='k9forum_cn'


#####################################################
##########     选择部署的环境	#####################
#####################################################
	echo "1) development (开发人员自己部署)"
	echo "2) sbpre (美国区用sandbox)"
	echo "3) sandbox (美国区发往sandbox和sbpre)"
	echo "4) master (发往online)"
read -p "chice deploy envirenment 环境:" env
	[ "x$env" == "x1" ] && env='development'
	[ "x$env" == "x2" ] && env='sbpre'
	[ "x$env" == "x3" ] && env='sandbox'
	[ "x$env" == "x4" ] && env='master'



function rsync_remote(){
	local git_url branch host remotedir app env region 
	host=$1
	remotedir=$2
	app=$3
	env=$4
	region=$5
	eval git_url=\$${app}Git
	if [ -d `dirname $0`/temp/$app ] 
	then
	  	cd `dirname $0`/temp/$app 
		git pull 
	else
		cd `dirname $0`/temp/ 
	 	git clone $git_url
	 	cd $app 
	fi
	if [ "x$env" == "xdevelopment" -o "x$env" == "xsbpre" -o "x$env" == "xsandbox" ]
	then
		branch="development"
	else
		branch="master"
	fi	
	git checkout $branch
	if [ "x$app" == "xk9game" -o "x$app" == "xmonster" ]
	then
		rsync -lrzgoatpv  --exclude-from="/opt/.list" ../$app/ $host:$remotedir/$app/ #&> /var/log/deploy.forum.log
	else
		git_modify $env $work_dir/temp/$app
		[ -e images ] &&  chmod -R  777 images || { mkdir images ; chmod -R 777 images ; }
		[ -e cache ] &&  chmod -R 777 cache || { mkdir cache ; chmod -R 777 cache ; }
		rsync -lrzgoatpv  --exclude-from="/opt/.list" ../$app/ $host:$remotedir/$app/
	fi
	echo `pwd`
	git reset --hard
}
rsync_remote "$host" "$remotedir" "$app" "$env" "$region" 
echo rsync_remote "$host" "$remotedir" "$app" "$env" "$region" 

