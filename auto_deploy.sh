#!/bin/bash
work_dir="/home/ec2-user/k9deploy/deploy_server/deploysh"
. $work_dir/conf/global.conf
. $work_dir/lib/*

cd $work_dir

function rsync_remote(){
        local git_url branch host remotedir app env region
        host=$1
        app=$2
        env=$3
        region=$4
	remotedir=$5
	if [ "$1" == "" -o "$2" == "" -o "$3" == "" -o "$4" == "" -o "$5" == ""  ]
	then
		echo "params should be 5 个."
		exit 5
	fi
        eval git_url=\$${app}Git
        if [ -d `dirname $0`/temp/$app ]
        then
                cd `dirname $0`/temp/$app
                git pull
        else
                cd `dirname $0`/temp/
                git clone $git_url
                cd $app
        fi
        if [ "x$env" == "xcndev" -o "x$env" == "xdevelopment" -o "x$env" == "xsbpre" -o "x$env" == "xsandbox" ]
        then
                branch="development"
        else
                branch="master"
        fi
        if [ "x$app" == "xk9game" -o "x$app" == "xmonster" ]
        then
                rsync -lrzgoatpv  --exclude-from="/opt/.list" ../$app/ $host:$remotedir/$app/
	else
                git_modify $env $work_dir/temp/$app
                [ -e cache ] &&  chmod -R 777 cache || { mkdir cache ; chmod -R 777 cache ; }
                [ -e images ] &&  chmod -R  777 images || { mkdir images ; chmod -R 777 images ; }
                rsync -lrzgoatpv  --exclude-from="/opt/.list" ../$app/ $host:$remotedir/$app/
        fi
        echo `pwd`
        git reset --hard
}

echo rsync_remote $1 $2 $3 $4

rsync_remote $1 $2 $3 $4  $remotedir



