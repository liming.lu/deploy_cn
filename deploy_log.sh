#!/bin/bash
if echo $1 |grep -P "\d+" &> /dev/null
then
        num=$1
        ssh kick9_dev "tail -n $num /home/ec2-user/k9deploy/deploy_server/deployServerPhp/log/log.txt"
else
	ssh kick9_dev "tail -f /home/ec2-user/k9deploy/deploy_server/deployServerPhp/log/log.txt"
fi
