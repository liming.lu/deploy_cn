#!/bin/bash
remotedir='/home/ec2-user'
cd /home/ec2-user/deploycode/k9deployshell

#####################################################
##########     host   #####################
#####################################################
#       echo "1) kick9_sbpre"
#       echo "2) kick9_sandbox"
#       echo "3) kick9_online"
#       echo "4) kick9_ope"
#read -p "choice deploy host :" host
#       [ "x$host" == "x1" ] && host='kick9_sbpre'
#       [ "x$host" == "x2" ] && host='kick9_sandbox'
#       [ "x$host" == "x3" ] && host='kick9_online'
#       [ "x$host" == "x4" ] && host='kick9_ope'

#####################################################
##########     region  #####################
#####################################################
#       echo "1) cn"
#       echo "2) tc"
#       echo "3) us"
#read -p "choice deploy region :" region
#       [ "x$region" == "x1" ] && region='cn'
#       [ "x$region" == "x2" ] && region='tc'
#       [ "x$region" == "x3" ] && region='us'

#####################################################
##########     app   #####################
#####################################################
        echo "1) k9pf(online)"
        echo "2) k9pay(online)"
        echo "3) k9ope"
        echo "4) k9openpf(online)"
        echo "5) k9lg(online)"
        echo "6) k9analysis"
        echo "7) k9cs"
        echo "8) k9dev"
        echo "9) k9mail"
        echo "10) k9nba"
        echo "11) k9official"
        echo "12) k9game"
        echo "13) k9forum_us"
        echo "14) k9ope_inner"
read -p "choice deploy application :" app
        [ "x$app" == "x1" ] && app='k9pf'
        [ "x$app" == "x2" ] && app='k9pay'
        [ "x$app" == "x3" ] && app='k9ope'
        [ "x$app" == "x4" ] && app='k9openpf'
        [ "x$app" == "x5" ] && app='k9lg'
        [ "x$app" == "x6" ] && app='k9analysis'
        [ "x$app" == "x7" ] && app='k9cs'
        [ "x$app" == "x8" ] && app='k9dev'
        [ "x$app" == "x9" ] && app='k9mail'
        [ "x$app" == "x10" ] && app='k9nba'
        [ "x$app" == "x11" ] && app='k9official'
        [ "x$app" == "x12" ] && app='k9game'
        [ "x$app" == "x13" ] && app='k9forum_us'
        [ "x$app" == "x14" ] && app='k9ope_inner'

#####################################################
##########     branch(env)  #####################
#####################################################
#       echo "1) development"
#       echo "2) sbpre"
#       echo "3) sandbox"
#       echo "4) master"
#read -p "chice deploy envirenment :" env
#       [ "x$env" == "x1" ] && env='development'
#       [ "x$env" == "x2" ] && env='sbpre'
#       [ "x$env" == "x3" ] && env='sandbox'
#       [ "x$env" == "x4" ] && env='master'




#####################################################
##########     log   #####################
#####################################################
read -p "give commite value log:" commit
        [ -n $commit ] && commit=$commit

host=kick9_sandbox
region=us

if [ "x$commit" == "x" ]
then
        echo "bash -x deploy.sh -p $host:$remotedir -a $app -e sandbox -r $region "
        bash -x -v deploy.sh -p $host:$remotedir -a $app -e sandbox -r $region
        if [ "x$host" == "xkick9_online" ]
        then
                echo "bash -x -v deploy.sh -p kick9_ope:$remotedir -a $app -e sandbox -r $region"
                bash -x -v deploy.sh -p kick9_ope:$remotedir -a $app -e sandbox -r $region
        fi
fi

if [ "x$commit" != "x" ]
then
        echo "bash -x deploy.sh -p $host:$remotedir -a $app -e sandbox -r $region -c $commit"
        bash -x -v deploy.sh -p $host:$remotedir -a $app -e sandbox -r $region -c $commit
        if [ "x$host" == "xkick9_online" ]
        then
                echo "bash -x -v deploy.sh -p kick9_ope:$remotedir -a $app -e sandbox -r $region -c $commit"
                bash -x -v deploy.sh -p kick9_ope:$remotedir -a $app -e sandbox -r $region -c $commit
        fi
fi

